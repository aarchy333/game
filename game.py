##Moving rectangle
import time
import pygame
import sys

gameover = False
size = width, height = 700, 700
screen = None
bg = None
rect = None


def handle_events():
    global gameover, rect
    for event in pygame.event.get():
        if event.type == pygame.KEYUP:
            rect.set_shift_to_zero()
        if event.type == pygame.QUIT:
            gameover = True


def init():
    global screen, bg
    pygame.init()
    bg = (255, 255, 255)
    screen = pygame.display.set_mode(size)


class Rjuman:
    def __init__(self, imagename, tlpoint, w, h, speed):
        self.img = pygame.image.load(imagename)
        self.img = pygame.transform.scale(self.img, (w, h))
        self.imgrect = self.img.get_rect()
        self.imgrect.x = tlpoint[0]
        self.imgrect.y = tlpoint[1]
        self.speed = speed


class Rectangle:
    def __init__(self, pos=(100, 100), size=(200, 50), color=(255, 0, 0)):
        self.pos = list(pos)
        self.size = size
        self.color = color
        self.xshift = 0
        self.yshift = 0

    def change_shift(self, dshift=(0, 0)):
        self.xshift += dshift[0]
        self.yshift += dshift[1]

    def set_shift_to_zero(self):
        self.xshift = 0
        self.yshift = 0

    def move(self):
        global width, height
        if self.pos[0] + self.xshift < 0 or self.pos[0] + self.size[0] + self.xshift > width:
            self.xshift *= -1
        if self.pos[1] + self.yshift < 0 or self.pos[1] + self.size[1] + self.yshift > height:
            self.yshift *= -1
        self.pos[0] += self.xshift
        self.pos[1] += self.yshift

    def draw(self):
        global screen
        pygame.draw.rect(screen, self.color, (self.pos[0], self.pos[1], self.size[0], self.size[1]))


def main():
    global rect
    curr_time = time.time()
    global gameover, screen, bg
    init()
    rect = Rectangle()
    bl_time = time.time()
    rjuman = Rjuman("rju.png", (200, 100), 180, 150, [1, 2])
    while not gameover:
        handle_events()
        screen.fill(bg)
        if time.time() - curr_time > 0.001:
            keys = pygame.key.get_pressed()
            if keys[pygame.K_a]:
                rect.change_shift(dshift=(-0.05, 0))
            if keys[pygame.K_d]:
                rect.change_shift(dshift=(0.05, 0))
            if keys[pygame.K_w]:
                rect.change_shift(dshift=(0, -0.05))
            if keys[pygame.K_s]:
                rect.change_shift(dshift=(0, 0.05))
            rect.move()
            rect.draw()
            curr_time = time.time()
        if time.time() - bl_time > 0.001:
            rjuman.imgrect.x += rjuman.speed[0]
            rjuman.imgrect.y += rjuman.speed[1]
            if rjuman.imgrect.x + rjuman.imgrect.width > width:
                rjuman.speed[0] *= -1
            if rjuman.imgrect.x < 0:
                rjuman.speed[0] *= -1
            if rjuman.imgrect.y + rjuman.imgrect.height > height:
                rjuman.speed[1] *= -1
            if rjuman.imgrect.y < 0:
                rjuman.speed[1] *= -1
            bl_time = time.time()
            screen.blit(rjuman.img, rjuman.imgrect)
        pygame.display.flip()

if __name__=="__main__":
    main()
